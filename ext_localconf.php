<?php

defined('TYPO3') or die('Access denied.');

/***************
 * Add custom RTE configuration

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default'] = 'EXT:cogtail_publishing_child/Configuration/RTE/Default.yaml';

 */

$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:frontend/Resources/Private/Language/locallang_tca.xlf'][] = 'EXT:cogtail_blogchild/Resources/Private/Language/locallang_tca.xlf';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:cogtail_blogchild/Resources/Private/Language/locallang_tca.xlf'][] = 'EXT:cogtail_blogchild/Resources/Private/Language/de.locallang_tca.xlf';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:core/Resources/Private/Language/locallang_csh_pages.xlf'][] = 'EXT:cogtail_blogchild/Resources/Private/Language/locallang_csh_pages.xlf';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:cogtail_blogchild/Resources/Private/Language/locallang_csh_pages.xlf'][] = 'EXT:cogtail_blogchild/Resources/Private/Language/de.locallang_csh_pages.xlf';
